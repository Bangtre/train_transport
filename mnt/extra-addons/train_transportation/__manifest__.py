# -*- coding: utf-8 -*-
{
    'name': "train_transportation",

    'summary': """
        make a small train schedule etc""",

    'description': """
        With this application youcan add, view, delete a schedule of a train station
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/partner_view.xml',
        'views/views.xml',
        'views/templates.xml',
        'views/menu_item.xml',
        'views/sequence_data.xml',
        'wizard/train_wizard.xml',
        
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
