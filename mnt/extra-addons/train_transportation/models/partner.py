from odoo import api, models, fields

class ResPartner(models.Model):
    _inherit = 'res.partner'
    
    train_state = fields.Selection([
        ('passenger', 'Passenger'), ('machinist', 'Machinist')
    ], string='Train State')
    identity = fields.Char('Identity')
    gender = fields.Selection([
        ('male', 'Male'), ('female', 'Female')
    ], string='Gender')
    born_date = fields.Date('Born Date')