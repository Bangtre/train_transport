from odoo import models, fields, api
from datetime import timedelta, datetime, date

class TrainCity(models.Model):
    _name = 'train.city'
    _description = 'Train City'

    name = fields.Char('Name')
    code = fields.Char('Code')
    station_ids = fields.One2many('train.station', 'city_id', string='station')

class TrainStation(models.Model):
    _name = 'train.station'
    _description = 'Train Station'
    # _inherits = {'train.city': 'city_id'}

    code = fields.Char('Code')
    name = fields.Char('Name')
    city_id = fields.Many2one('train.city', string='City', ondelete='cascade')
    address = fields.Text('Address')
    origin_ids = fields.One2many('train.schedule', 'origin_id', string='schedule')
    destination_ids = fields.One2many('train.schedule', 'destination_id', string='schedule')

class TrainTrain(models.Model):
    _name = 'train.train'

    name = fields.Char('Name')
    code = fields.Char('Code')
    capacity = fields.Integer('Capacity')
    state = fields.Selection([
        ('ready', 'Ready'), ('notready', 'Not Ready'), ('mt', 'Maintenance')
    ], string='State')
    active = fields.Boolean('active', default=True)
    schedule_line = fields.One2many('train.schedule', 'train_id', string='schedule_line')
    capacity_line = fields.One2many('train.schedule', 'capacity_id', string='capacity')
class TrainSchedule(models.Model):
    _name = 'train.schedule'

    code = fields.Char('Code', readonly=True, default='/')
    origin_id = fields.Many2one('train.station', string='origin')
    destination_id = fields.Many2one('train.station', string='destination')
    schedule_time = fields.Datetime('Schedule Time')
    duration = fields.Float('Duration')
    arrival_time = fields.Datetime('Arrival Time', compute='compute_arrival_time')
    train_id = fields.Many2one('train.train', string='Train', required=True)
    capacity_id = fields.Many2one('train.train', string='Capacity')

    
    @api.depends('schedule_time', 'duration')
    def compute_arrival_time(self):
        for arrival in self:
            if not arrival.schedule_time:
                arrival.arrival_time = arrival.schedule_time
                continue
            start = fields.Date.from_string(arrival.schedule_time)
            arrival.arrival_time = start + timedelta(days=arrival.duration)

    def set_arrival_time(self):
        for arrival in self:
            if not (arrival.schedule_time and arrival.arrival_time):
                continue
            
            schedule_time = fields.Datetime.from_string(arrival.schedule_time)
            duration = fields.Datetime.from_string(arrival.duration)
            arrival.duration = (schedule_time + duration).days

    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('train.schedule') 
        return super(TrainSchedule, self).create(vals)