from odoo import api, fields, models

class TrainWizard(models.TransientModel):
    _name = 'train.wizard'
    _description = 'Train Schedule Wizard'

    def _default_sesi(self):
        return self.env['train.train'].browse(self._context.get('active_ids'))

    train_id = fields.Many2one('train.train', string='Schedule', default=_default_sesi)
    schedule_line = fields.Many2many('train.schedule', string='Train')
    train_ids = fields.Many2many('train.train', string='Schedule', default=_default_sesi)
    

    def add_schedules(self):
        for train in self.train_ids:
            train.schedule_line |= self.schedule_line

    def add_schedule(self):
        self.train_id.schedule_line |= self.schedule_line

